package example.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * Created by a.hofmann on 15.05.2016.
 */
public abstract class PersistenceUtils {
   //-----------------------------------------------------------------------------------------------------------------

   private static volatile PersistenceUtils INSTANCE;

   //-----------------------------------------------------------------------------------------------------------------

   public static PersistenceUtils getInstance() {
      return getInstance("defaultPU");
   }
   public static PersistenceUtils getInstance(String persistenceUnitName) {
      if(INSTANCE == null) {
         //https://en.wikipedia.org/wiki/Double-checked_locking
         synchronized (PersistenceUtils.class) {
            if(INSTANCE == null) {
               INSTANCE = new PersistenceUtils(persistenceUnitName){};
            }
         }
      }
      return INSTANCE;
   }

   public static void overrideDefault(PersistenceUtils persistenceUtils) {
      INSTANCE = Objects.requireNonNull(persistenceUtils, "parameter is null");
   }

   //-----------------------------------------------------------------------------------------------------------------

   public static EntityManager getEntityManager() {
      return getInstance().get();
   }

   //-----------------------------------------------------------------------------------------------------------------

   protected final EntityManagerFactory entityManagerFactory;
   protected volatile EntityManager entityManager;

   //-----------------------------------------------------------------------------------------------------------------

   public PersistenceUtils(String persistenceUnitName) {
      this(persistenceUnitName, Collections.emptyMap());
   }

   public PersistenceUtils(String persistenceUnitName, Map<String, Object> parameters) {
      this.entityManagerFactory = initialize(persistenceUnitName, parameters);
   }

   //-----------------------------------------------------------------------------------------------------------------

   protected EntityManagerFactory initialize(String persistenceUnitName, Map<String, Object> parameters) {
      return Persistence.createEntityManagerFactory(persistenceUnitName, parameters);
   }

   //-----------------------------------------------------------------------------------------------------------------

   public EntityManager get() {
      return get(Collections.emptyMap());
   }

   public EntityManager get(Map<String, Objects> properties) {
      if(entityManager == null) {
         this.entityManager = this.entityManagerFactory.createEntityManager(properties);
      }
      return entityManager;
   }

   public void close() {
      if(entityManager != null) {
         entityManager.close();
         entityManager = null;
      }
   }

   //-----------------------------------------------------------------------------------------------------------------

   public static void transactional(Runnable task) {
      transactional(()-> {
         task.run();
         return null;
      });
   }

   public static <V> V transactional(Callable<V> task) {
      return transactional (
         task,
         null,
         (e) -> {
            throw new IllegalStateException(e);
         }
      );
   }

   public static <V> V transactional(Callable<V> task, V defaultValue, Consumer<Exception> failed) {
      final EntityTransaction tx = getEntityManager().getTransaction();
      if(!tx.isActive()) {
         tx.begin();
      }

      try {
         V result = task.call();
         tx.commit();
         return result;
      } catch (Exception e) {
         if(tx.isActive()) {
            tx.rollback();
         }
         failed.accept(e);
      }

      return defaultValue;
   }
}
