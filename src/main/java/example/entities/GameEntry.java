package example.entities;

import example.entities.spec.GeneratedIdEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by 666 on 02.06.2016.
 */
@Table(name = "game_entry")
@Entity(name = "GameEntry")
public class GameEntry extends GeneratedIdEntity<Integer> {
   private static final long serialVersionUID = 1L;
   //----------------------------------------------------------------------------------------------------------------

   @ManyToOne
   @JoinColumn(name = "gid")
   private Game game;

   @ManyToOne
   @JoinColumn(name = "qid")
   private Question question;

   @ManyToOne
   @JoinColumn(name = "aid")
   private Answer answer;

   //----------------------------------------------------------------------------------------------------------------

   public Game getGame() {
      return game;
   }

   public GameEntry setGame(Game game) {
      this.game = game;
      return this;
   }

   public Question getQuestion() {
      return question;
   }

   public GameEntry setQuestion(Question question) {
      this.question = question;
      return this;
   }

   public Answer getAnswer() {
      return answer;
   }

   public GameEntry setAnswer(Answer answer) {
      this.answer = answer;
      return this;
   }
}
