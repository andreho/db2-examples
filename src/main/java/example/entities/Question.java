package example.entities;

import example.entities.spec.ProvidedIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 666 on 02.06.2016.
 */
@Entity(name = "Question")
public class Question extends ProvidedIdEntity<String> {
   private static final long serialVersionUID = 1L;
   //-----------------------------------------------------------------------------------------------------------------

   @ManyToOne
   private Category category;
   @JoinColumn(name = "qid")
   @ManyToMany(cascade = {CascadeType.ALL})
   private List<Answer> answers;

   @ManyToOne
   @JoinColumn(name = "right_aid")
   private Answer rightAnswer;

   //----------------------------------------------------------------------------------------------------------------

   public Question() {
   }

   public Question(String id) {
      super(id);
   }

   //----------------------------------------------------------------------------------------------------------------

   public Question initialize() {
      return this.setAnswers(new ArrayList<>());
   }

   //----------------------------------------------------------------------------------------------------------------

   public Category getCategory() {
      return category;
   }

   public Question setCategory(Category category) {
      this.category = category;
      return this;
   }

   public List<Answer> getAnswers() {
      return answers;
   }

   public Question setAnswers(List<Answer> answers) {
      this.answers = answers;
      return this;
   }

   public Answer getRightAnswer() {
      return rightAnswer;
   }

   public Question setRightAnswer(final Answer rightAnswer) {
      this.rightAnswer = rightAnswer;
      return this;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public String toString() {
      return "Question{"+getId()+"}";
   }
}
