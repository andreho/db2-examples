package example.entities;

import example.entities.spec.ProvidedIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 666 on 02.06.2016.
 */
@Entity(name = "Category")
public class Category extends ProvidedIdEntity<String> {
   private static final long serialVersionUID = 1L;
   //-----------------------------------------------------------------------------------------------------------------

   @OneToMany(mappedBy = "category", cascade = {CascadeType.ALL})
   private List<Question> questions;

   //-----------------------------------------------------------------------------------------------------------------

   public Category() {
   }

   public Category(String s) {
      super(s);
   }

   //----------------------------------------------------------------------------------------------------------------

   public Category initialize() {
      this.setQuestions(new ArrayList<>());
      return this;
   }

   //----------------------------------------------------------------------------------------------------------------

   public List<Question> getQuestions() {
      return questions;
   }

   public Category setQuestions(List<Question> questions) {
      this.questions = questions;
      return this;
   }

   //----------------------------------------------------------------------------------------------------------------


   @Override
   public String toString() {
      return getId() + " (" + getQuestions().size()+")";
   }
}
