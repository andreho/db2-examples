package example.entities;

import example.entities.spec.ProvidedIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

/**
 * Created by 666 on 02.06.2016.
 */
@Entity(name = "Answer")
public class Answer extends ProvidedIdEntity<String> {
   private static final long serialVersionUID = 1L;
   //-----------------------------------------------------------------------------------------------------------------

   public Answer() {
   }

   public Answer(final String id) {
      super(id);
   }

   //----------------------------------------------------------------------------------------------------------------

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof Answer)) {
         return false;
      }

      Answer answer = (Answer) o;

      return Objects.equals(getId(), answer.getId());
   }

   @Override
   public int hashCode() {
      return Objects.hashCode(getId());
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public String toString() {
      return "Answer{"+getId()+"}";
   }
}
