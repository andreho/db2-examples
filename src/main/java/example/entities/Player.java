package example.entities;

import example.entities.spec.ProvidedIdEntity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 666 on 02.06.2016.
 */
@Entity(name = "Player")
public class Player extends ProvidedIdEntity<String> {
   private static final long serialVersionUID = 1L;
   //-----------------------------------------------------------------------------------------------------------------

   @OneToMany(mappedBy = "player")
   private List<Game> games;

   //-----------------------------------------------------------------------------------------------------------------

   public Player() {
   }

   public Player(String s) {
      super(s);
   }

   //-----------------------------------------------------------------------------------------------------------------

   public Player initialize() {
      this.setGames(new ArrayList<>());
      return this;
   }

   //-----------------------------------------------------------------------------------------------------------------

   public List<Game> getGames() {
      return games;
   }

   public Player setGames(List<Game> games) {
      this.games = games;
      return this;
   }
}
