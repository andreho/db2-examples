package example.entities;

import example.entities.spec.GeneratedIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 666 on 02.06.2016.
 */
@Entity(name = "Game")
public class Game extends GeneratedIdEntity<Integer> {
   private static final long serialVersionUID = 1L;
   //----------------------------------------------------------------------------------------------------------------

   @Column(name = "start_")
   @Temporal(TemporalType.TIMESTAMP)
   private Date start;
   @Column(name = "end_")
   @Temporal(TemporalType.TIMESTAMP)
   private Date end;
   @ManyToOne
   @JoinColumn(name = "pid")
   private Player player;
   @OneToMany(mappedBy = "game", cascade = {CascadeType.ALL})
   private List<GameEntry> entries;

   //----------------------------------------------------------------------------------------------------------------

   public Game initialize() {
      this.setEntries(new ArrayList<>());
      return this;
   }

   //----------------------------------------------------------------------------------------------------------------

   public Date getStart() {
      return start;
   }

   public Game setStart(Date start) {
      this.start = start;
      return this;
   }

   public Date getEnd() {
      return end;
   }

   public Game setEnd(Date end) {
      this.end = end;
      return this;
   }

   public Player getPlayer() {
      return player;
   }

   public Game setPlayer(Player player) {
      this.player = player;
      return this;
   }

   public List<GameEntry> getEntries() {
      return entries;
   }

   public Game setEntries(List<GameEntry> entries) {
      this.entries = entries;
      return this;
   }
}
