package example.entities.spec;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by 666 on 02.06.2016.
 */
@MappedSuperclass
@Access(AccessType.FIELD)
public class GeneratedIdEntity<ID extends Serializable> extends AbstractEntity<ID> {
   //-----------------------------------------------------------------------------------------------------------------

   @Id
   @GeneratedValue
   private ID id;

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public ID getId() {
      return id;
   }

   @Override
   protected void setId(ID id) {
      this.id = id;
   }
}
