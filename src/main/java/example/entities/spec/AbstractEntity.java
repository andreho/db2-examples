package example.entities.spec;

import java.io.Serializable;

/**
 * Created by 666 on 02.06.2016.
 */
public abstract class AbstractEntity<ID extends Serializable> implements Serializable {
   public abstract ID getId();

   protected void setId(ID id) {
      //NO-OP
   }
}
