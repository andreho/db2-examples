package example.entities.spec;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by 666 on 02.06.2016.
 */
@MappedSuperclass
@Access(AccessType.FIELD)
public abstract class ProvidedIdEntity<ID extends Serializable> extends AbstractEntity<ID> {
   @Id
   private ID id;

   //-----------------------------------------------------------------------------------------------------------------

   protected ProvidedIdEntity() {
   }

   public ProvidedIdEntity(ID id) {
      this.id = id;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public ID getId() {
      return id;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof ProvidedIdEntity)) {
         return false;
      }
      ProvidedIdEntity<?> other = (ProvidedIdEntity<?>) o;
      return Objects.equals(getId(), other.getId());
   }

   @Override
   public int hashCode() {
      return Objects.hashCode(getId());
   }
}
