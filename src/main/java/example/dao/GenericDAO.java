package example.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by a.hofmann on 18.05.2016.
 */
public interface GenericDAO<T extends Serializable> {
   T findById(Serializable id);

   List<T> findAll();

   long count();

   void persist(T value);

   void persistAll(Collection<T> values);

   void update(T value);

   void updateAll(Collection<T> values);
}
