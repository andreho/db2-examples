package example.readers;


import example.entities.Category;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by 666 on 23.04.2016.
 */
public interface DataReader {
   /**
    * Reads given file and extracts provided {@link Category categories}
    *
    * @param csvFile with content
    * @return a list with categories with their questions and answers
    * @throws IOException
    */
   List<Category> readFile(File csvFile) throws IOException;
}
