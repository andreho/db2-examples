package example.readers.impl;

import example.entities.Answer;
import example.entities.Category;
import example.entities.Question;
import example.readers.DataReader;
import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.hofmann on 04.05.2016.
 */
public class CsvDataReaderImpl implements DataReader {

   private final Map<String, Category> categoryMap;

   //-----------------------------------------------------------------------------------------------------------------

   public CsvDataReaderImpl() {
      this(new LinkedHashMap<>());
   }

   public CsvDataReaderImpl(final Map<String, Category> categoryMap) {
      this.categoryMap = categoryMap;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public List<Category> readFile(File csvFile) throws IOException {
      assert csvFile.getName().endsWith(".csv") : "Expected a *.csv file.";

      List<String> lines;

      try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(csvFile))) {
         lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
      }

      return processLines(lines);
   }

   //-----------------------------------------------------------------------------------------------------------------

   private List<Category> processLines(List<String> lines) throws IOException {
      if (lines.isEmpty()) {
         return Collections.emptyList();
      }

      final Iterator<String> iterator = lines.iterator();
      iterator.next();

      String line = "";
      try {
         while (iterator.hasNext()) {
            line = iterator.next();
            final String[] split = line.split(";");
            processLine(split);
         }
      } catch (Exception e) {
         throw new IOException("Unable to process given line: '" + line + "'", e);
      }

      return new ArrayList<>(categoryMap.values());
   }

   private void processLine(String[] line) {
//      final int qid = Integer.parseInt(line[0]);

      final String questionText = line[1];
      final String answer1 = line[2];
      final String answer2 = line[3];
      final String answer3 = line[4];
      final String answer4 = line[5];
      final int solution = Integer.parseInt(line[6]);
      final String categoryName = line[7];

      final Category category = categoryMap.computeIfAbsent(categoryName, (key) -> new Category(key).initialize());
      final Question question = new Question(questionText).initialize();

      question.getAnswers().add(new Answer(answer1));
      question.getAnswers().add(new Answer(answer2));
      question.getAnswers().add(new Answer(answer3));
      question.getAnswers().add(new Answer(answer4));

      question.setRightAnswer(question.getAnswers().get(solution - 1));

      int index = category.getQuestions().indexOf(question);
      if(index > -1) {
         category.getQuestions().remove(index);
      }

      question.setCategory(category);
      category.getQuestions().add(question);
   }
}
