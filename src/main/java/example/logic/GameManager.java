package example.logic;

import example.entities.Answer;
import example.entities.Category;
import example.entities.Game;
import example.entities.GameEntry;
import example.entities.Player;
import example.entities.Question;
import example.persistence.PersistenceUtils;
import example.readers.impl.CsvDataReaderImpl;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by 666 on 02.06.2016.
 */
public class GameManager {
   //-----------------------------------------------------------------------------------------------------------------

   private final InputStream input;
   private final PrintStream output;

   private boolean running;
   private boolean valid;
   private String text;
   private Object value;
   private Scanner scanner;
   private GameState gameState;

   private Map<String, Category> categories = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
   private Map<String, Player> players = new HashMap<>();
   private Set<Question> questions = new HashSet<>();

   //-----------------------------------------------------------------------------------------------------------------

   public GameManager() {
      this(System.in, System.out);
   }

   public GameManager(InputStream in, PrintStream out) {
      this.input = Objects.requireNonNull(in, "input stream is null");
      this.output = Objects.requireNonNull(out, "output stream is null");

      this.scanner = new Scanner(in);
   }

   //-----------------------------------------------------------------------------------------------------------------

   public InputStream getInput() {
      return input;
   }

   public PrintStream getOutput() {
      return output;
   }

   public String getText() {
      return text;
   }

   public GameState getGameState() {
      return gameState;
   }

   public Map<String, Category> getCategories() {
      return categories;
   }

   public Map<String, Player> getPlayers() {
      return players;
   }

   public Set<Question> getQuestions() {
      return questions;
   }

   //-----------------------------------------------------------------------------------------------------------------

   public void run(String[] args) {
      output.println("Welcome to Quiz-game v.0.5");
      this.running = true;
      changeState(GameState.MENU);

      while (this.running) {
         consume(
               Integer.class,
               (s) -> s.matches("[0-4]"),
               Integer::parseInt,
               (menuIdx) -> {
                  switch (menuIdx) {
                     case 0:
                        this.running = false;
                        break;
                     case 1:
                        loadCsv();
                        break;
                     case 2:
                        registerUser();
                        break;
                     case 3:
                        startNewGame();
                        break;
                     case 4:
                        showStatistics();
                        break;
                  }
               },
               "Invalid input: ${input}",
               null
         );
      }
      output.println("Good bye.");
   }

   //-----------------------------------------------------------------------------------------------------------------

   private void showStatistics() {

   }

   private void startNewGame() {
      Player player = selectPlayer();
      if(player == null) {
         changeState(GameState.MENU);
         return;
      }

      if(categories.isEmpty()) {
         List<Category> resultList = PersistenceUtils.getEntityManager().createQuery("SELECT c FROM Category c").getResultList();
         categories.putAll(resultList.stream().collect(Collectors.toMap(category -> category.getId(), category -> category)));
      }

      final List<Category> categories = selectCategories();
      final List<Question> randomQuestions = selectQuestions(categories);
      Collections.shuffle(randomQuestions, ThreadLocalRandom.current());

      final Game game = new Game().initialize()
                                  .setPlayer(player)
                                  .setStart(new Date());

      changeState(GameState.ASKING);
      for(Question question : randomQuestions) {
         output.println(question.getCategory().getId() + ": " + question.getId());
         final List<Answer> answers = question.getAnswers();
         int i = 0;
         for(Answer answer : answers) {
            output.println(++i + ": " + answer.getId());
         }

         while(true) {
            if(scanner.hasNextInt()) {
               int answerIdx = scanner.nextInt();
               if(answerIdx > 0 && answerIdx <= answers.size()) {
                  Answer answer = answers.get(answerIdx - 1);

                  GameEntry entry = new GameEntry()
                        .setGame(game)
                        .setAnswer(answer)
                        .setQuestion(question);

                  game.getEntries().add(entry);

                  if(answer.equals(question.getRightAnswer())) {
                     output.println("Correct choice.");
                  } else {
                     output.println("Wrong choice.");
                  }

                  break;
               } else {
                  output.println("Invalid index of an answer: "+answerIdx);
               }
            }
         }
      }

      game.setEnd(new Date());

      PersistenceUtils.transactional(()-> {
         PersistenceUtils.getEntityManager().persist(game);
      });
      output.println("Previous game was stored successfully.");
      changeState(GameState.MENU);
   }

   private Player selectPlayer() {
      changeState(GameState.PLAYER_SELECT);
      final AtomicReference<Player> selectedPlayer = new AtomicReference<>();
      consume(
         Player.class,
         (name) -> {
            Player player = PersistenceUtils.getEntityManager().find(Player.class, name);
            if(player == null) {
               return false;
            }
            selectedPlayer.set(player);
            return true;
         },
         (name) -> selectedPlayer.get(),
         (player) -> {},
         "Username ${input} doesn't exist. Register first.",
         GameState.CATEGORIES_SELECT
      );
      return selectedPlayer.get();
   }

   private List<Category> selectCategories() {
      final Set<Category> selectedCategories = new LinkedHashSet<>();
      output.println("Select at least two categories:");

      while(true) {
         changeState(GameState.CATEGORIES_SELECT);
         output.println("To complete selection - input 'end'");
         String input = this.text = scanner.next();
         Category category;

         if("end".equalsIgnoreCase(input)) {
            if(selectedCategories.size() >= 2) {
               break;
            } else {
               output.println("Select at least two categories.");
            }
         }

         if(input == null || input.isEmpty()){
            output.println("Invalid name of a category: '"+input+"'");
         } else if((category = categories.get(input)) == null) {
            output.println("Category not found: '"+input+"'");
         } else if (!selectedCategories.add(category)) {
            output.println("Category was already added: '"+input+"'");
         }
      }
      return new ArrayList<>(selectedCategories);
   }

   private List<Question> selectQuestions(final List<Category> categories) {
      changeState(GameState.QUESTIONS);
      final ThreadLocalRandom random = ThreadLocalRandom.current();
      final List<Question> selectedQuestions = new ArrayList<>();
      final Set<Question> addedQuestions = new HashSet<>();

      for(Category category : categories) {
         final List<Question> questions = category.getQuestions();
         loop:
         while(true) {
            output.println("Enter count of questions for '" + category.getId() + "', min. 1 and max. " +
                           questions.size() + ": ");
            if(scanner.hasNextInt()) {
               int count = scanner.nextInt();
               if(count < 1 || count > questions.size()) {
                  output.println("Invalid count of questions "+count+" for category: "+category.getId());
                  continue loop;
               } else {
                  for(int i = 0, l = questions.size() * 10; i < l; i++) {
                     if(count <= 0 ) {
                        break loop;
                     }
                     Question question = questions.get(random.nextInt(questions.size()));
                     if(addedQuestions.add(question)) {
                        count--;
                     }
                  }
               }
            } else {
               output.println("Invalid input: "+scanner.next());
            }
         }
         selectedQuestions.addAll(addedQuestions);
         addedQuestions.clear();
      }
      return selectedQuestions;
   }

   private void loadCsv() {
      changeState(GameState.LOAD_CSV);
      consume(Path.class,
              (path) -> new File(path).exists(),
              (path) -> Paths.get(path),
              (path) -> {
                 try {
                    Files.walkFileTree(path, new FileVisitor<Path>() {
                       @Override
                       public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs)
                             throws IOException {
                          new CsvDataReaderImpl(categories).readFile(file.toAbsolutePath().toFile());
                          return FileVisitResult.CONTINUE;
                       }

                       @Override
                       public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs)
                             throws IOException {
                          return FileVisitResult.CONTINUE;
                       }

                       @Override
                       public FileVisitResult postVisitDirectory(final Path dir, final IOException exc)
                             throws IOException {
                          return FileVisitResult.CONTINUE;
                       }

                       @Override
                       public FileVisitResult visitFileFailed(final Path file, final IOException exc)
                             throws IOException {
                          throw exc;
                       }
                    });
                 } catch (IOException e) {
                    e.printStackTrace();
                 }
                 PersistenceUtils.transactional(() -> {
                    final EntityManager entityManager = PersistenceUtils.getEntityManager();
                    for (Category category : categories.values()) {
                       entityManager.merge(category);
                       output.println("Storing/updating category: "+category.getId());
                    }
                 });
              },
              "Unable to find given file/directory: ${input}",
              GameState.MENU
      );
   }

   private void registerUser() {
      changeState(GameState.PLAYER_REG);
      consume(
         String.class,
         (name) -> true,
         (name) -> name,
         (name) -> {
            Player player = PersistenceUtils.getEntityManager().find(Player.class, name);
            if(player != null) {
               output.println("Username '"+name+"' already exists.");
            } else {
               PersistenceUtils.transactional(() -> {
                  PersistenceUtils.getEntityManager().persist(new Player(name).initialize());
                  output.println("Username '"+name+"' was created.");
               });
            }
         },
         "Invalid username: ${input}",
         GameState.MENU
      );
   }

   //-----------------------------------------------------------------------------------------------------------------

   private <V> boolean consume(Class<V> type,
                               Predicate<String> predicate,
                               Function<String, V> converter,
                               Consumer<V> success,
                               String failedMessage,
                               GameState nextState) {
      String str = this.text = this.scanner.next();
      if (str == null ||
          str.isEmpty() ||
          !predicate.test(str)) {
         this.valid = false;
         complain(failedMessage, str);
         return false;
      }

      V value = type.cast(converter.apply(str));
      this.value = value;
      success.accept(value);
      this.valid = true;
      if(nextState != null) {
         changeState(nextState);
      }
      return true;
   }

   private void complain(String message, String input) {
      if(message != null && !message.isEmpty()) {
         output.println(message.replaceFirst("\\$\\{input\\}", input));
      }
   }

   private void changeState(GameState state) {
      this.gameState = state;
      print(state);
   }

   //-----------------------------------------------------------------------------------------------------------------

   private void print(GameState state) {
      switch (state) {
         case MENU: {
            output.println(
                  "1. Load from CSV\n" +
                  "2. Register an user\n" +
                  "3. Start a new game\n" +
                  "4. Show statistics\n" +
                  "0. Exit"
            );
         } break;

         case LOAD_CSV: {
            output.println("Input path to a CSV-file/directory: ");
         } break;
         case CATEGORIES_SELECT: {
            for (Category category : categories.values()) {
               output.println(category.getId());
            }
         } break;
         case PLAYER_SELECT:
         case PLAYER_REG: {
            output.println("Input a username: ");
         } break;
      }
   }
}
