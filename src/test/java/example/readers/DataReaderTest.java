package example.readers;

import example.entities.Category;
import example.entities.Question;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

//import example.readers.impl.CsvDataReaderImpl;

/**
 * Created by 666 on 23.04.2016.
 */
public class DataReaderTest {
   //Place the data for your test into /src/test/resources folder with the following filename: Wissenstest_sample200.csv
   static final URI TEST_FILE_URI = getResource("/Wissenstest_sample200.csv");

   private static final String LINE_DELIMITER = "\n";
   private static final String ELEMENT_DELIMITER = ";";

   private static DataReader dataReader;

   //----------------------------------------------------------------------------------------------------------------
   private List<Category> categories;

   //----------------------------------------------------------------------------------------------------------------

   private static URI getResource(String resourceName) {
      try {
         return DataReaderTest.class.getResource(resourceName).toURI();
      } catch (URISyntaxException e) {
         throw new IllegalStateException("Unable to provide URI to the given resource: " + resourceName, e);
      }
   }

   //----------------------------------------------------------------------------------------------------------------

   @BeforeClass
   public static void setUpClass() throws URISyntaxException {
      DataReaderTest.dataReader = csvFile -> (Collections.emptyList());
   }

   @Before
   public void setUp() throws Exception {
      this.categories = dataReader.readFile(new File(TEST_FILE_URI));
   }

   //----------------------------------------------------------------------------------------------------------------

   @Test
   public void expectedCategoriesCount51() throws Exception {
      assertEquals(51, categories.size());
   }

   @Test
   public void expectedQuestionsCount200() throws Exception {
      Set<Question> visitedQuestions = new HashSet<>();
      categories.forEach((category) ->
                               visitedQuestions.addAll(category.getQuestions()));
      assertEquals(200, visitedQuestions.size());
   }

   @Test
   public void eachQuestionHasAnId() throws Exception {
      categories.forEach(
            (category) ->
                  category.getQuestions().forEach(
                        (question) -> assertNotNull(question.getId())
                  )
      );
   }

   @Test
   public void eachQuestionHasFourPossibleAnswers() throws Exception {
      categories.forEach((category) -> category.getQuestions().forEach(
            (question) -> assertEquals(4, question.getAnswers().size())));
   }

   @Test
   public void eachQuestionHasOneRightAnswer() throws Exception {
      categories.forEach((category) -> category.getQuestions().forEach(
            (question) -> assertNotNull(question.getRightAnswer())));
   }

   @Test
   public void eachQuestionContainsRightAnswerInItsAnswersList() throws Exception {
      categories.forEach((category) -> category.getQuestions().forEach(
            (question) ->
                  assertTrue(question.getAnswers().contains(question.getRightAnswer()))));
   }

   @Test
   public void eachQuestionBelongsToTheSameCategory() throws Exception {
      categories.forEach((owningCategory) -> owningCategory.getQuestions().forEach(
            (question) ->
                  assertEquals(owningCategory, question.getCategory())
      ));
   }

   //----------------------------------------------------------------------------------------------------------------

   // and so on ...
}
